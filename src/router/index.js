import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Sobremi from '@/components/Sobremi'
import Section from '@/components/Section'
import Recipes from '@/components/Recipes'
import Contact from '@/components/Contact'
import Single from '@/components/Single'
import SingleLast from '@/components/SingleLast'
import Search from '@/components/Search'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/sobre-mi',
      name: 'Sobremi',
      component: Sobremi
    },
    {
      path: '/seccion/:id',
      name: 'Section',
      component: Section
    },
    {
      path: '/recipes/:id',
      name: 'Recipes',
      component: Recipes
    },
    {
      path: '/contacto',
      name: 'Contact',
      component: Contact
    },
    {
      path: '/blog/:id',
      name: 'Single',
      component: Single
    },
    {
      path: '/blog/articulo/:id',
      name: 'SingleLast',
      component: SingleLast
    },
    {
      path: '/search/:text?',
      name: 'Search',
      component: Search
    }
  ],
  scrollBehavior (to, from, savedPosition) {
    return {x: 0, y: 0}
  }
})
